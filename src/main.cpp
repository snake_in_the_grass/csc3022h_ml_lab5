#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <map>
#include <iomanip>

// ------------------------------------------------------------------------
// CLASS: State
// ------------------------------------------------------------------------
class State
{
    private:
        int id;
        std::map<int, double> reward_model;
        std::vector<int> neighbor_ids;
        double  new_value;

    public:
        double value;
        void findNewValue(std::vector<State *> grid_states){
            double temp_value = 0;

            for(int id : neighbor_ids){
                temp_value = 0.8 * grid_states[id-1]->value + reward_model[id];

                new_value = (temp_value > value) ? temp_value : new_value;
            }
        }

        bool updateValue(){
            if( value < new_value ){
                value = new_value;
                new_value = 0;
                return true;
            }else{
                new_value = 0;
                return false;
            }
            
        }

    friend std::ostream &operator<<(std::ostream &os, const State &s);
    friend class Gridworld;
};


// ------------------------------------------------------------------------
// EXTERNAL OVVERIDE METHODS
// ------------------------------------------------------------------------
std::ostream &operator<<(std::ostream &os, const State &s){
    os << "State (" << s.id << "): " << s.value ;
    return os;
}

// ------------------------------------------------------------------------
// CLASS: Gridworld
// ------------------------------------------------------------------------
class Gridworld
{
    private:
    std::vector<State*> states;

    public:
    Gridworld(){
        populateWorld(); 
    }

    void populateWorld(){
        // Create 1
        State * state1 = new State();
        state1->id = 1;
        state1->reward_model[2] = 0;
        state1->reward_model[4] = 0;
        state1->neighbor_ids.push_back(2);
        state1->neighbor_ids.push_back(4);
        // Add 1
        states.push_back(state1);

        // Create 2 
        State * state2 = new State();
        state2->id = 2;
        state2->reward_model[1] = 0;
        state2->reward_model[5] = 0;
        state2->reward_model[3] = 50;
        state2->neighbor_ids.push_back(1);
        state2->neighbor_ids.push_back(3);
        state2->neighbor_ids.push_back(5);
        // Add 2
        states.push_back(state2);

        // Create 3
        State * state3 = new State();
        state3->id = 3;
        // Add 3
        states.push_back(state3);

        // Create 4
        State * state4 = new State();
        state4->id = 4;
        state4->reward_model[1] = 0;
        state4->reward_model[5] = 0;
        state4->neighbor_ids.push_back(1);
        state4->neighbor_ids.push_back(5);
        // Add 4
        states.push_back(state4);

        // Create 5
        State * state5 = new State();
        state5->id = 5;
        state5->reward_model[2] = 0;
        state5->reward_model[4] = 0;
        state5->reward_model[6] = 0;
        state5->neighbor_ids.push_back(2);
        state5->neighbor_ids.push_back(4);
        state5->neighbor_ids.push_back(6);
        // Add
        states.push_back(state5);

        // Create 6
        State * state6 = new State();
        state6->id = 6;
        state6->reward_model[3] = 100;
        state6->reward_model[5] = 0;
        state6->neighbor_ids.push_back(3);
        state6->neighbor_ids.push_back(5);
        // Add 6
        states.push_back(state6);
    }

    void valueIteration(std::ofstream &output_file){
        bool not_converged = true;
        int it_counter = 0;

        while(not_converged)
        {
            not_converged = false;
            
            output_file << "Iteration (" << it_counter++ << ")\n" << *this << std::endl;
            std::cout << "Iteration (" << it_counter << ")\n" << *this << std::endl;

            for(State * state : states)
                state->findNewValue(states);
            
            for(State * state : states)
                not_converged = (state->updateValue() || not_converged);   
        }
    }

    

    friend std::ostream &operator<<(std::ostream &os, const Gridworld &g);
};

// ------------------------------------------------------------------------
// EXTERNAL OVVERIDE METHODS
// ------------------------------------------------------------------------

std::ostream &operator<<(std::ostream &os, const Gridworld &g){
    os << std::setprecision(1);
    os << std::fixed;
    os << " ----------------------------\n";
    for(int i = 1; i <= g.states.size(); ++i){
        if( i%3 ==0 )
            os << " | " << std::setw(6) << g.states[i-1]->value << " |\n";
        else
            os << " | " << std::setw(6) << g.states[i-1]->value ;        
    }
    os << " ----------------------------\n";
    return os;
}



// ------------------------------------------------------------------------
// METHOD: Main
// ------------------------------------------------------------------------
int main()
{
    using namespace std;
    string filename = "./outputs/output.txt";
    ofstream output_file(filename);
    if (!output_file)
    {
        cout << "Could not open " + filename << endl;
    }

    Gridworld grid;
    grid.valueIteration(output_file);
    
    return 0;
}