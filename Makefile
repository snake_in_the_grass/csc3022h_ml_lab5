CC=g++
FLAGS=-std=c++11
SRC=./src
OBJ=./src/obj
INCLUDE_DIR=./include/
# DEPS=$(INCLUDE_DIR)<>.h

OBJECTS=$(addprefix $(OBJ)/, \
	main.o )

value_iteration: $(OBJECTS) $(DEPS)
	$(CC) $(FLAGS) $(OBJECTS) -I $(INCLUDE_DIR) -o $@ 


# $(OBJ)/pca.o:$(SRC)/pca.cpp
	# $(CC) -c -o $@ $< $(FLAGS) -I $(INCLUDE_DIR)

$(OBJ)/main.o:$(SRC)/main.cpp
	$(CC) -c -o $@ $< $(FLAGS) -I $(INCLUDE_DIR)	

clean:
	rm $(OBJ)/*.o 