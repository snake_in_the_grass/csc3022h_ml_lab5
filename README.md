# CSC3022H - Lab 5 - Value Iteration

## Project Structure

- `output/` 
    - contains the output text file
- `src/`
    - contains the driver file and source code (a single file)
    - `obj/`
- `Makefile`
    - Makefile
- `value_iteration`
    - The executable


## How to make

From project root directory run command:
```
make
```


## How to run

From project root directory run command:
```
./value_iteration
```
