# CSC3022 Lab 5 Answers


---
## Question 1
4 steps were required for convergence to:

```
Iteration (4)
 ----------------------------
 |   51.2 |   64.0 |    0.0 |
 |   64.0 |   80.0 |  100.0 |
 ----------------------------
 ```

Please refer to file:
```
./outputs/output.txt
```
For the value function of each state at each iteration of the algorithm. 

----
## Question 2

s1 -> s2 -> s5 -> s6 -> s3

__OR__

s1 -> s4 -> s5 -> s6 -> s3

----
## Question 3

Yes this is possible. One could change the reward given for action `(1,1) -> (2,1)`. This reward would have to be greater than 64 to change V*, but smaller than 100 to keep the policy the same. 

For example, if `R[(1,1) -> (2,1)] = 79`, the final V* would be:
```
----------------------------
|   63.2 |   79.0 |    0.0 |
|   64.0 |   80.0 |  100.0 |
----------------------------
 ```

 The optimal policy is still:  
 
 s1 -> s2 -> s5 -> s6 -> s3

 However, there are no longer two optimal policies. It is not possible to change V* and still have two optimal policies. As soon as `R[(1,1) -> (2,1)] > 64`, the second policy:
 
 s1 -> s4 -> s5 -> s6 -> s3

 Is not longer valid since V(s2) > V(s4).
